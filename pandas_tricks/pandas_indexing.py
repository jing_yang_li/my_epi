
# https://www.geeksforgeeks.org/select-rows-columns-by-name-or-index-in-pandas-dataframe-using-loc-iloc/

# import pandas
import pandas as pd

# List of Tuples
employees = [('Stuti', 28, 'Varanasi', 20000),
             ('Saumya', 32, 'Delhi', 25000),
             ('Aaditya', 25, 'Mumbai', 40000),
             ('Saumya', 32, 'Delhi', 35000),
             ('Saumya', 32, 'Delhi', 30000),
             ('Saumya', 32, 'Mumbai', 20000),
             ('Aaditya', 40, 'Dehradun', 24000),
             ('Seema', 32, 'Delhi', 70000)
             ]

# Create a DataFrame object from list
df = pd.DataFrame(employees,
                  columns =['Name', 'Age',
                            'City', 'Salary'])

# Set 'Name' column as index on a Dataframe
df.set_index(["Name", "Age"], inplace = True)
# df.set_index("Name", inplace=True)


# Using the operator .loc[] to select multiple rows whose index value is
# "Stuti" or "Seema" with some particular columns
result = df.loc[["Stuti", "Seema"], ["City", "Salary"]]
print(result)

#-------------------------------------------
# https://www.sharpsightlabs.com/blog/pandas-index/
sales_data = pd.DataFrame({
    "name":["William","Emma","Sofia","Markus","Edward","Thomas","Ethan","Olivia","Arun","Anika","Paulo"]
    ,"region":["East","North","East","South","West","West","South","West","West","East","South"]
    ,"sales":[50000,52000,90000,34000,42000,72000,49000,55000,67000,65000,67000]
    ,"expenses":[42000,43000,50000,44000,38000,39000,42000,60000,39000,44000,45000]})

# default index
print(sales_data.index)
# RangeIndex(start=0, stop=11, step=1)

# re-define the index of the DataFrame.
# set the name variable as the index.
sales_data.set_index('name', inplace = True)
print(sales_data)

'''
        region  sales  expenses
name                           
William   East  50000     42000
Emma     North  52000     43000
Sofia     East  90000     50000
Markus   South  34000     44000
Edward    West  42000     38000
'''
# the name column is now set off to the left side.

print(sales_data.index)
'''
Index(['William', 'Emma', 'Sofia', 'Markus', 'Edward', 'Thomas', 'Ethan',
       'Olivia', 'Arun', 'Anika', 'Paulo'],
      dtype='object', name='name')
'''
# At this point, the name variable is no longer a column exactly. It has become the index of the DataFrame.
# This means that we can use the values of this new index to retrieve the rows of the DataFrame.

# The loc method enables you to select rows by a defined index.
# we’re going to use the Pandas .loc method to select a single row of data from our DataFrame.
print(sales_data.loc['Markus', :])

# use the loc method and reference a range of names from our index.
# use .loc[] to retrieve all of the rows from ‘Markus‘ to ‘Thomas‘.
print(sales_data.loc['Markus':'Thomas', :])
'''
       region  sales  expenses
name                          
Markus  South  34000     44000
Edward   West  42000     38000
Thomas   West  72000     39000
'''

# reset index
#  remove the index from the DataFrame and make the name variable back into an ordinary column.
# The .reset_index() method takes the existing index removes it. By default, when it removes
# the index, .reset_index() turns the index back into a regular column.
sales_data.reset_index(inplace = True)
print(sales_data)