"""
Pandas DataFrame apply() function is used to apply a function along an axis of the DataFrame
"""

"""
Applying a Function to DataFrame Elements
The DataFrame on which apply() function is called remains unchanged. 
The apply() function returns a new DataFrame object after applying the function to its elements.
"""

import pandas as pd

df = pd.DataFrame({'A': [1, 2], 'B': [10, 20]})
def square(x):
    return x * x
df1 = df.apply(square)
print(df)
print(df1)

