"""
The Python pandas DataFrame.expanding() method.
This is one of the window methods of pandas and it provides expanding transformations.
It returns a window sub-classed for the particular operation.
"""

"""
In the below example, the DataFrame.expanding() method calculated the cumulative 
sum of the entire DataFrame.
"""
import pandas as pd
df = pd.DataFrame({"A": [1, 2, 3],"B": [1, 1, 1]})
print("---The DataFrame is---")
print(df)
print("------Output of the function is-------")
print(df.expanding().sum())

"""
In the below example, the DataFrame.expanding() method calculated the cumulative sum of 
the selected column in the DataFrame and store the result in the other column.
"""
df = pd.DataFrame({"A": [1, 2, 3],"B": [1, 1, 1]})
print("---The DataFrame is---")
print(df)
print("------Output of the function is-------")
df["result"] = df.A.expanding().sum()
print(df)

"""
In the below example, the DataFrame.expanding() method calculated the cumulative sum of 
the entire DataFrame along the row axis.
"""
df = pd.DataFrame({"A": [1, 2, 3],"B": [1, 1, 1]})
print("---The DataFrame is---")
print(df)
print("------Output of the function is-------")
print(df.expanding(axis=1).sum())

"""
In the below example the DataFrame.expanding() method calculated the cumulative sum of 
the entire DataFrame with min_periods=2.
"""
import pandas as pd
df = pd.DataFrame({"A": [1, 2, 3],"B": [1, 1, 1]})
print("---The DataFrame is---")
print(df)
print("------Output of the function is-------")
print(df.expanding(min_periods=2).sum())