import pandas as pd

def double(r):
    return r['Max Speed']*2

df = pd.DataFrame({'Animal': ['Falcon', 'Falcon',
                              'Parrot', 'Parrot'],
                   'Max Speed': [380., 370., 24., 26.]})

df = df.groupby(['Animal'])

# the operations will be applied to each group
# m = df.mean()
# m = df.sum()
m = df.apply(lambda row: sum(double(row)))

print(m)


print(286/(1.079)**1 + 286/(1.079)**2)