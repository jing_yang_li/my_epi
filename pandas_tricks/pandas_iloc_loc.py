import pandas as pd

country_data_dict = {
    'country':['USA', 'China', 'Japan', 'Germany', 'UK', 'India']
    ,'continent':['Americas','Asia','Asia','Europe','Europe','Asia']
    ,'GDP':[19390604, 12237700, 4872137, 3677439, 2622434, 2597491]
    ,'population':[322179605, 1403500365, 127748513, 81914672, 65788574, 1324171354]
}
country_data_df = pd.DataFrame(country_data_dict, columns = ['country', 'continent', 'GDP', 'population'])

# let’s just select the first row of data.
print(country_data_df.iloc[0])
'''
country             USA
continent      Americas
GDP            19390604
population    322179605
Name: 0, dtype: object
'''

# a different way to select a single row using iloc. : for all columns
print(country_data_df.iloc[0, :])

# retrieve a single column and all rows we need to use a special syntax using the ‘:‘ character.
print(country_data_df.iloc[: ,0])
'''
0        USA
1      China
2      Japan
3    Germany
4         UK
5      India
Name: country, dtype: object
'''

# select a single cell
print(country_data_df.iloc[2,0])
'''
Japan
'''

# retrieve subset of data
print(country_data_df.iloc[1:5,0:3])
'''
   country continent       GDP
1    China      Asia  12237700
2    Japan      Asia   4872137
3  Germany    Europe   3677439
4       UK    Europe   2622434
'''
#---------------------------------------------
# loc method

#==========================
# CREATE DICTIONARY OF DATA
#==========================
country_data_dict = {
    'country':['USA', 'China', 'Japan', 'Germany', 'UK', 'India']
    ,'continent':['North America','Asia','Asia','Europe','Europe','Asia']
    ,'GDP':[19390604, 12237700, 4872137, 3677439, 2622434, 2597491]
    ,'population':[322179605, 1403500365, 127748513, 81914672, 65788574, 1324171354]
}
country_data_df = pd.DataFrame(country_data_dict, columns = ['country', 'continent', 'GDP', 'population'])

#use the values of one of our existing columns, country, as the row labels.
country_data_df = country_data_df.set_index('country')
print(country_data_df )
'''
             continent       GDP  population
country                                     
USA      North America  19390604   322179605
China             Asia  12237700  1403500365
Japan             Asia   4872137   127748513
Germany         Europe   3677439    81914672
UK              Europe   2622434    65788574
India             Asia   2597491  1324171354
'''

print(country_data_df.loc['USA'])
'''
continent     North America
GDP                19390604
population        322179605
Name: USA, dtype: object
'''
print(country_data_df.loc['USA',:])

# select column
print(country_data_df.loc[:,'population'])
'''
ountry
USA         322179605
China      1403500365
Japan       127748513
Germany      81914672
UK           65788574
India      1324171354
Name: population, dtype: int64
'''

# select a specific cell
print(country_data_df.loc['China', 'GDP'])

# slice
print(country_data_df.loc['China':'Germany', :])
'''
       continent       GDP  population
country                                
China        Asia  12237700  1403500365
Japan        Asia   4872137   127748513
Germany    Europe   3677439    81914672
'''

print(country_data_df.loc['China':'USA', :])
'''
Empty DataFrame
Columns: [continent, GDP, population]
Index: []
'''

print(country_data_df.loc[:, 'GDP':'population'])
'''
              GDP  population
country                      
USA      19390604   322179605
China    12237700  1403500365
Japan     4872137   127748513
Germany   3677439    81914672
UK        2622434    65788574
India     2597491  1324171354
'''

# subset
print(country_data_df.loc['China':'Germany', 'GDP':'population'])
'''
              GDP  population
country                      
China    12237700  1403500365
Japan     4872137   127748513
Germany   3677439    81914672
'''

#------------------------------------------
# multiple coloumns as index
country_data_df.reset_index(inplace=True)
country_data_df = country_data_df.set_index(['country', 'continent'])
print(country_data_df)
'''
                            GDP  population
country continent                          
USA     North America  19390604   322179605
China   Asia           12237700  1403500365
Japan   Asia            4872137   127748513
Germany Europe          3677439    81914672
UK      Europe          2622434    65788574
India   Asia            2597491  1324171354
'''

df = country_data_df.loc['USA', 'North America']
print(df)
'''
GDP            19390604
population    322179605
'''

print(df['GDP'])
'''
19390604
'''

df = country_data_df.loc[('China', 'Asia')]
print(df)
'''
GDP             12237700
population    1403500365
Name: (China, Asia), dtype: int64
'''

df = country_data_df.loc[('China', 'Asia'), 'GDP']
print(df)
'''
12237700
'''
df = country_data_df.loc[('China'), 'GDP']

# change value
country_data_df.loc[('China'), 'GDP'] = 123
print(country_data_df)

print(country_data_df.memory_usage())
print(country_data_df.memory_usage().sum())