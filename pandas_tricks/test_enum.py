from enum import Enum

class RapdrDataQueryParam(str, Enum):
    COMMODITY = "commodity"
    REGION = "region"
    LOCATION = "location"
    PEAK_TYPE = "peaktype"
    COB_DATE = "cobdate"
    COB_DATE_2 = "cobdate2"
    USER_SLID = "userSlid"

needed_query_params = [
    enum
    for enum in RapdrDataQueryParam
    if enum not in (RapdrDataQueryParam.REGION,)
]

print(needed_query_params)

for field in needed_query_params:
    print(field)