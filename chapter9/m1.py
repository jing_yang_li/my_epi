'''
Longest Palindromic Substring

Given a string s, return the longest palindromic substring in s.

Example 1:

Input: s = "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: s = "cbbd"
Output: "bb"
Example 3:

Input: s = "a"
Output: "a"
Example 4:

Input: s = "ac"
Output: "a"

Constraints:

1 <= s.length <= 1000
s consist of only digits and English letters (lower-case and/or upper-case),

'''

'''
In fact, we could solve it in O(n^2) time using only constant space.

We observe that a palindrome mirrors around its center. Therefore, a palindrome can be 
expanded from its center, and there are only 2n - 12n−1 such centers.

You might be asking why there are 2n - 12n−1 but not nn centers? The reason is the center 
of a palindrome can be in between two letters. Such palindromes have even number of letters 
(such as "abba") and its center are between the two 'b's.
'''


class Solution:

    @classmethod
    def longestPalindrome(cls, s: str) -> str:

        if s is None or len(s) < 1:
            return ""

        start = 0
        end = 0
        for i in range(len(s)):
            len1 = cls.expandAroundCenter(s, i, i) # using i as center
            len2 = cls.expandAroundCenter(s, i, i + 1) # using space between i and i+1 as center
            l = max(len1, len2)

            # update the start and end of longest substring
            if l > end - start:
                start = int(i - (l - 1) / 2)
                end = int(i + l / 2)
            print(l)

        return s[start : (end + 1)]

    @classmethod
    def expandAroundCenter(cls, s, left, right):
        l = left
        r = right
        while l >= 0 and r < len(s) and s[l] == s[r]:
            l -= 1
            r += 1

        return r - l - 1


s = "bcabacd"
p = Solution.longestPalindrome(s)
print(p)