

def spiralOrder(matrix):
    ans = []

    if (len(matrix) == 0):
        return ans

    # matrix size
    R = len(matrix)
    C = len(matrix[0])

    # seen denotes that the cell on the r-th row and c-th column was previously visited
    seen = [[0 for i in range(C)] for j in range(R)]

    # moving direction, right, down, left, up
    dr = [0, 1, 0, -1]
    dc = [1, 0, -1, 0]

    # current position
    r = 0
    c = 0

    # current direction
    di = 0

    # Iterate from 0 to R * C - 1
    for i in range(R * C):
        ans.append(matrix[r][c])
        seen[r][c] = True

        # candidate’s next position
        cr = r + dr[di]
        cc = c + dc[di]

        # if it is in the bounds and unseen, set it to the next position
        if (0 <= cr and cr < R and 0 <= cc and cc < C and not(seen[cr][cc])):
            r = cr
            c = cc
        else:
            #  next position is the one after performing a clockwise turn
            di = (di + 1) % 4
            r += dr[di]
            c += dc[di]
    return ans


def spiralPrint(m, n, a):
    k = 0
    l = 0

    ''' k - starting row index
        m - ending row index
        l - starting column index
        n - ending column index
        i - iterator '''

    while (k < m and l < n):

        # Print the first row from the remaining rows
        for i in range(l, n):
            print(a[k][i], end=" ")

        k += 1

        # Print the last column from the remaining columns
        for i in range(k, m):
            print(a[i][n - 1], end=" ")

        n -= 1

        # Print the last row from
        # the remaining rows
        if (k < m):

            for i in range(n - 1, (l - 1), -1):
                print(a[m - 1][i], end=" ")

            m -= 1

        # Print the first column from the remaining columns
        if (l < n):
            for i in range(m - 1, k - 1, -1):
                print(a[i][l], end=" ")

            l += 1


# Driver code
a = [[1, 2, 3, 4],
     [5, 6, 7, 8],
     [9, 10, 11, 12],
     [13, 14, 15, 16]]

for x in spiralOrder(a):
    print(x, end=" ")

'''
Time Complexity: O(N), where NN is the total number of elements in the input matrix. 
    We add every element in the matrix to our final answer.
    
Space Complexity: O(N), the information stored in seen and in ans.
'''