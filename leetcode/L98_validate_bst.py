
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def isValidBST(self, root) -> bool:

        def validate(root):
            if root.left is None and root.right is None:
                return True, root.val, root.val

            if root.left and root.right is None:
                ls, lmin, lmax = validate(root.left)
                return (root.val > lmax) and ls, min(lmin, root.val), max(lmax, root.val)
            elif root.right and root.left is None:
                rs, rmin, rmax = validate(root.right)
                return (root.val < rmin)  and rs, min(rmin, root.val), max(rmax, root.val)
            else:
                ls, lmin, lmax = validate(root.left)
                rs, rmin, rmax = validate(root.right)
                return ls and rs and (root.val > lmax) and (root.val < rmin), min(lmin, rmin, root.val), max(lmax, rmax, root.val)

        status, _, _ = validate(root)
        return status

print(6*11.95)
