from typing import List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:

    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        ans = []

        def level_travel(root):
            if root.val is None:
                return []

            tmp = [root.val]
            if root.left is not None:
                tmp.extend(level_travel(root.left))

            if root.right is not None:
                tmp.extend(level_travel(root.right))

            ans.append(tmp)
            return ans

        if root is not None: level_travel(root)

        return ans


t1 = TreeNode(val=9)
t3 = TreeNode(val=15)
t4 = TreeNode(val=7)
t2 = TreeNode(val=20, left=t3, right=t4)
root = TreeNode(val=3, left=t1, right=t2)
solution = Solution()
ans = solution.levelOrder(root)
print(ans)
