
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def detectCycle1(self, head):
        visited = []
        tmp = head
        while tmp:
            visited.append(tmp)
            tmp = tmp.next
            if tmp in visited:
                return tmp

        return None

    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        slow = head
        fast = head

        while fast and fast.next:

            # move slow by one
            slow = slow.next

            # move fast by two
            fast = fast.next.next
            print(slow.val, fast.val)
            # if they meet any node, the linked list contains a cycle
            if slow == fast:
                # reset head
                fast = head
                while fast != slow:
                   fast = fast.next
                   slow = slow.next
                return slow

        # we reach here if slow and fast do not meet
        return None

node1 = ListNode(3)
node2 = ListNode(2)
node3 = ListNode(0)
node4 = ListNode(4)
# node4.next = node2
# node3.next = node4
# node2.next = node3
node1.next = node2
node2.next = node1

sol = Solution()
hasCycle = sol.detectCycle(node1)
print(hasCycle.val)
