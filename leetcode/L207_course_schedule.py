
# class Solution:
#     def dfs(self, v, visit, gr):
#         if visit[v] == 1:
#             return True
#         visit[v] = -1
#         for i in gr[v]:
#             if visit[i] == -1 or not self.dfs(i, visit, gr):
#                 return False
#         visit[v] = 1
#         return True
#
#     # @param {integer} numCourses
#     # @param {integer[][]} prerequisites
#     # @return {boolean}
#     def canFinish(self, numCourses, prerequisites):
#         # gr is a matrix, row i is the list of courses that require i
#         gr = [[] for x in range(numCourses)]
#         visit = [0] * numCourses  # status of each course
#
#         # row i stores the list of courses that require i
#         for p in prerequisites:
#             if p[0] not in gr[p[1]]:
#                 gr[p[1]].append(p[0])
#
#         # dfs checking for each unvisited vertex
#         for v in range(numCourses):
#             if visit[v] != 1:
#                 if not self.dfs(v, visit, gr):
#                     return False
#         return True

# refer to Kahn's algorithm
class Solution:
    def canFinish(self, numCourses, prerequisites):
        map = [[] for x in range(numCourses)]  # the directed graph
        ind = [0 for x in range(numCourses)]   # the indegree of each vertex

        # populate the graph and indegree status
        # row i is the list of courses that require course i
        for p in prerequisites:
            if p[0] not in map[p[1]]:
                ind[p[0]] += 1
                map[p[1]].append(p[0])

        # list of vertices with 0 indegree
        st = []
        for i in range(numCourses):
            if ind[i] == 0:
                st.append(i)

        count = 0   # number of vertices with 0 indegree after updating
        while st:
            tmp = st.pop(0)
            count += 1
            # for each neighbor that depends on tmp
            for i in map[tmp]:
                ind[i] -= 1  # decrease indegree by 1
                if ind[i] == 0:  # if its indegree is 0, add it to the list
                    st.append(i)

        # if n of vertices with 0 indegree is less than total n of courses, it must be cyclic
        if count < numCourses:
            return False
        else:
            return True

numCourses = 3
prerequisites = [[1, 0], [1, 2], [0, 1]]
sol = Solution()
result = sol.canFinish(numCourses, prerequisites)
print(result)
# False