# Python3 program to
# Print all combinations
# of balanced parentheses

# Wrapper over _printParenthesis()
def printParenthesis(str, n):
    # if(n > 0):
    #     _printParenthesis(str, 0, n, 0, 0)
    # return

    def generate(str, pos, n, open, close):

        if(close == n):
            # for i in str:
            #     print(i, end="")
            # print()
            ans.append("".join(str))
            return
        else:
            if(open > close):
                # print('a', pos)
                str[pos] = '}'
                generate(str, pos + 1, n, open, close + 1)
            if(open < n):
                # print('b', pos)
                str[pos] = '{'
                generate(str, pos + 1, n, open + 1, close)

    ans = []
    str = [""] * 2 * n
    if(n > 0):
        generate(str, 0, n, 0, 0)

    return ans


# Driver Code
n = 2
# str = [""] * 2 * n
ans = printParenthesis(str, n)
print(ans)

# str=['a', 'b', 'c']
# for s in str:
#     print(s, end="")