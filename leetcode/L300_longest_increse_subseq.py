
class Solution:
    def lengthOfLIS(self, nums) -> int:

        # lengths = [0]*len(nums)
        #
        # for i in range(len(nums)):
        #
        #     max_l = 0
        #     for j in range(i-1, -1, -1):
        #
        #         if lengths[j] > max_l and nums[i] > nums[j]:
        #             max_l = lengths[j]
        #             print(max_l)
        #     lengths[i] = max_l + 1
        #
        # return max(lengths)

        cache = {}

        def find(nums, i):

            if i == 0:
                cache[i] = 1
                return 1

            for j in range(i-1, -1, -1):
                if j not in cache:
                    cache[j] = find(nums, j)

            maxl = 0
            for j in range(i-1, -1, -1):
                if nums[j] < nums[i]:
                    if cache[j] > maxl:
                        maxl = cache[j]

            cache[i] = maxl + 1
            return maxl + 1

        find(nums, len(nums)-1)
        return max(cache.values())


nums = [0,1,0,3,2,3]
nums = [1,3,6,7,9,4,10,5,6]
# nums = [0,0,0]
sol = Solution()
result = sol.lengthOfLIS(nums)
print(result)