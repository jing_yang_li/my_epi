
from typing import List
#
# class Solution:
#     def pacificAtlantic(self, heights: List[List[int]]) -> List[List[int]]:
#         m = len(heights)
#         n = len(heights[0])
#
#         def bfs(heights, r, c):
#             m = len(heights)
#             n = len(heights[0])
#             queue = []
#             queue.append((r, c))
#             visited = set()
#             while queue:
#                 current = queue.pop(0)
#                 r, c = current[0], current[1]
#                 visited.add(current)
#                 east = None
#                 west = None
#                 north = None
#                 south = None
#
#                 # neighbors
#                 if c > 0:
#                     west = (r, c-1)
#
#                 if c < n-1:
#                     east = (r, c+1)
#
#                 if r > 0:
#                     north = (r-1, c)
#
#                 if r < m-1:
#                     south = (r+1, c)
#
#                 if east and (east not in visited) and heights[east[0]][east[1]] <= heights[current[0]][current[1]]:
#                     queue.append(east)
#                 if west and (west not in visited) and heights[west[0]][west[1]] <= heights[current[0]][current[1]]:
#                     queue.append(west)
#                 if north and (north not in visited) and heights[north[0]][north[1]] <= heights[current[0]][current[1]]:
#                     queue.append(north)
#                 if south and (south not in visited) and heights[south[0]][south[1]] <= heights[current[0]][current[1]]:
#                     queue.append(south)
#
#             return visited
#
#         def to_both_ocean(visited, m, n):
#             touch_p = False
#             for v in visited:
#                 if v[0] == 0 or v[1] == 0:
#                     touch_p = True
#                     break
#
#             touch_a = False
#             for v in visited:
#                 if v[0] == (m-1) or v[1] == (n-1):
#                     touch_a = True
#                     break
#
#             return touch_p and touch_a
#
#         results = []
#         for r in range(m):
#             for c in range(n):
#                 visited = bfs(heights, r, c)
#
#                 if to_both_ocean(visited, m, n):
#                     results.append([r, c])
#
#         return results
def pacificAtlantic(matrix: List[List[int]]) -> List[List[int]]:
    if not matrix or not matrix[0]:return []
    m, n = len(matrix),len(matrix[0])
    p_visited = set()
    a_visited = set()
    directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]
    def dfs(visited, x,y):
        visited.add((x,y))
        for dx, dy in directions:
            new_x, new_y = x+dx, y+dy
            if 0<=new_x<m and 0<=new_y<n and (new_x,new_y) not in visited and matrix[new_x][new_y]>=matrix[x][y]:
                dfs(visited, new_x,new_y)
    #iterate from left border and right border
    for i in range(m):
        dfs(p_visited,i,0)
        dfs(a_visited,i,n-1)
    #iterate from up border and bottom border
    for j in range(n):
        dfs(p_visited,0,j)
        dfs(a_visited,m-1,j)
    #The intersections of two sets are coordinates where water can flow to both P and A
    return list(p_visited.intersection(a_visited))

heights = [[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]

results = pacificAtlantic(heights)
print(results)

s = "hello"
print(s[0:0])
