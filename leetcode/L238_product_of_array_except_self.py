from typing import List

class ProductFinder:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        left = [1 for i in range(len(nums))]
        right = [1 for i in range(len(nums))]
        for i in range(1, len(left)):
            left[i] = left[i-1]*nums[i-1]
        for i in range(len(left)-2, -1, -1):
            right[i] = right[i+1]*nums[i+1]
        output = [1 for i in range(len(nums))]
        for i in range(len(nums)):
            output[i] = left[i] * right[i]
        return output
'''
for [1,2,3,4,5]
the left is: [1, 1, 2, 6, 24]
the right is: [120, 60, 20, 5, 1]
Time O(n)
Space O(n)
'''

nums = [1, 2, 3, 4, 5]
p = ProductFinder()
output = p.productExceptSelf(nums)
print(output)

