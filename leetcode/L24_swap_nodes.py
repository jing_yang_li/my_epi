
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def swap_pairs_iterative(self):
        temp = self.head

        if temp is None:
            return

        while(temp and temp.next):
            if(temp.data != temp.next.data):
                temp.data, temp.next.data = temp.next.data, temp.data
            temp = temp.next.next

    def swap_pairs_recursive(self):
        self.swap(self.head)

    def swap(self, head):
        if head and head.next:
            head.data, head.next.data = head.next.data, head.data
            self.swap(head.next.next)

    def push(self, new_data):
        new_node = Node(new_data)
        new_node.next = self.head
        self.head = new_node

    def print_list(self):
        temp = self.head
        while temp:
            print(temp.data)
            temp = temp.next

# Driver program
llist = LinkedList()
llist.push(5)
llist.push(4)
llist.push(3)
llist.push(2)
llist.push(1)

print("Linked list before calling pairWiseSwap() ")
llist.print_list()

llist.swap_pairs_iterative()
llist.swap_pairs_recursive()

print("\nLinked list after calling pairWiseSwap()")
llist.print_list()
