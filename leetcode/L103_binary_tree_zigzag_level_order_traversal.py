# A node structure
class Node:
    # A utility function to create a new node
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

# Iterative Method to print a binary tree in level order
def printLevelOrder(root):
    results = []
    # Base Case
    if root is None:
        return []

    # Create an empty queue # for level order traversal
    queue = []

    # Enqueue Root
    queue.append(root)
    left_to_right = True
    while len(queue) > 0:

        size = len(queue)
        tmp = []
        i = 0
        while i < size:
            i = i + 1
            # Print front of queue and remove it from queue
            node = queue.pop(0)
            tmp.append(node.val)
            if node:
                # Enqueue left child
                if node.left is not None:
                    queue.append(node.left)

                # Enqueue right child
                if node.right is not None:
                    queue.append(node.right)
        if left_to_right:
            results.append(tmp)
            left_to_right = False
        else:
            results.append(tmp[::-1])
            left_to_right = True

    return results

# Driver Program to test above function
root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

print ("Level Order Traversal of binary tree is -")
results = printLevelOrder(root)
print(results)
