from typing import List

# problem 17

class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        digit_letter_map = {
            '2': ['a', 'b', 'c'],
            '3': ['d', 'e', 'f'],
            '4': ['g', 'h', 'i'],
            '5': ['j', 'k', 'l'],
            '6': ['m', 'n', 'o'],
            '7': ['p', 'q', 'r', 's'],
            '8': ['t', 'u', 'v'],
            '9': ['w', 'x', 'y', 'z']
        }
        ans = []

        def combine(digits):
            if len(digits) == 0:
                return ""

            if len(digits) == 1:
                return digit_letter_map[str(digits[0])]

            combs = []
            for letter in digit_letter_map[str(digits[0])]:
                for c in combine(digits[1:]):
                    combs.append(letter+""+c)

            return combs

        combos = combine(digits)
        return combos


digits = "263"
sol = Solution()
ans = sol.letterCombinations(digits)
print(ans)