# Python3 implementation of the approach
from collections import deque

# https://www.geeksforgeeks.org/deque-in-python/

# Function to return a list that contains all the generated letter combinations

def letterCombinationsUtil(number, n, table):

    list = []
    q = deque()
    q.append("")

    while len(q) != 0:
        s = q.pop()

        # If s is a complete word push it in the list
        if len(s) == n:
            list.append(s)
        else:

            # Try to combine all possible letters for current digit in number[] with s.
            # Which number to combine with s depend on the length of s.
            #   if len(s) = 0, it will be number[0]
            #   if len(s) = 1, it will be number[1]
            print("pop ", s)
            for letter in table[number[len(s)]]:
                print("append ", s+letter)
                q.append(s + letter)

    return list


# Function that creates the mapping and
# calls letterCombinationsUtil
def letterCombinations(number, n):

    # table[i] stores all characters that
    # corresponds to ith digit in phone
    table = ["0", "1", "abc", "def", "ghi", "jkl",
             "mno", "pqrs", "tuv", "wxyz"]

    list = letterCombinationsUtil(number, n, table)

    print(list)
    return


# Driver code
number = [2, 3, 4]
n = len(number)

# Function call
letterCombinations(number, n)
