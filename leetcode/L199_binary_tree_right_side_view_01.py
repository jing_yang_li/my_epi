# https://www.techiedelight.com/print-right-view-binary-tree/

# A node structure
class Node:
    # A utility function to create a new node
    def __init__(self, key):
        self.val = key
        self.left = None
        self.right = None

# Iterative Method to print a binary tree in level order
def printRightView(root):
    results = []
    # Base Case
    if root is None:
        results

    # Create an empty queue for level order traversal
    queue = []

    # Enqueue root
    queue.append(root)

    while len(queue) > 0:
        # size of current level
        size = len(queue)
        i = 0

        while i < size:
            i = i + 1
            if i == size and queue[0]:
                # print(queue[0].data)
                results.append(queue[0].val)

            node = queue.pop(0)
            if node:
                # Enqueue left child
                if node.left is not None:
                    queue.append(node.left)

                # Enqueue right child
                if node.right is not None:
                    queue.append(node.right)

    return results

if __name__ == '__main__':

    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.right = Node(4)
    root.right.left = Node(5)
    root.right.right = Node(6)
    root.right.left.left = Node(7)
    root.right.left.right = Node(8)

    results = printRightView(root)
    print(results)

'''
Time: O(n)
Space: O(n)
'''