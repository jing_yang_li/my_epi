# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        visited = []
        tmp = head
        while tmp:
            visited.append(tmp)
            tmp = tmp.next
            if tmp in visited:
                return True

        return False

    def hasCycle2(self, head):
        fast = head
        slow = head.next
        while fast and slow:
            slow = slow.next
            if fast == slow:
                return True

            fast = fast.next
            slow = slow.next

        return False

node1 = ListNode(3)
node2 = ListNode(2)
node3 = ListNode(0)
node4 = ListNode(4)
node4.next = node2
node3.next = node4
node2.next = node3
node1.next = node2

sol = Solution()
hasCycle = sol.hasCycle2(node1)
print(hasCycle)
