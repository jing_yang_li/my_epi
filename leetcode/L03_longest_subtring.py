
def longest_bustring(s):
    if not s:
        return 0

    longest = s[0]

    i = 0
    j = 1
    while i < len(s) and j < len(s):
        if s[j] not in s[i:j]:
            j += 1
        else:
            # if len(s[i:j]) > len(longest):
            #     longest = s[i:j]
            i += 1
        if len(s[i:j]) > len(longest):
            longest = s[i:j]
    return len(longest)

s = "bbbbb"
s = "au"
print(longest_bustring(s))