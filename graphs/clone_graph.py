

# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []


class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':
        if node is None:
            return None

        queue = []
        queue.append(node)

        new_head = Node(node.val)
        clone_node_map = {node.val:new_head}  # used for keep track of visited nodes and copied nodes

        while queue:
            node = queue.pop(0)

            # visit neighbors
            for neighbor in node.neighbors:
                if neighbor.val not in clone_node_map:
                    clone_node = Node(neighbor.val)
                    # add it to the map
                    clone_node_map[neighbor.val] = clone_node
                    # add to queue
                    queue.append(neighbor)

                # add each neighbor to clone_node's neighbors
                clone_node_map[node.val].neighbors.append(
                    clone_node_map[neighbor.val]
                )

        return new_head


n2 = Node(2)
n0 = Node(0)
n1 = Node(1)
n3 = Node(3)
n2.neighbors = [n0, n1, n3]
n0.neighbors = [n2, n1]
n3.neighbors = [n2]

sol = Solution()
new_g = sol.cloneGraph(n2)
print(new_g.val)
for n in new_g.neighbors:
    print(n.val)

