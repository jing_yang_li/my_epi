
# This class represents a directed graph using adjacency list representation
class Graph:
    def __init__(self):
        self.graph = {}

    def add_edge(self, u, v):
        u_neighbors = self.graph.get(u, [])
        if u_neighbors:
            u_neighbors.append(v)
        else:
            self.graph[u] = [v]

    def bfs(self, start):
        # max(self.graph) gives the max node values, starting from 0
        queue = []
        visited = [False] * (max(self.graph) + 1)  # use node's value as index
        queue.append(start)
        visited[start] = True
        while queue:
            node = queue.pop(0)
            print(node)
            for neighbor in self.graph[node]:
                if not visited[neighbor]:
                    queue.append(neighbor)
                    visited[neighbor] = True

g = Graph()
g.add_edge(0, 1)
g.add_edge(0, 2)
g.add_edge(1, 2)
g.add_edge(2, 0)
g.add_edge(2, 3)
g.add_edge(3, 3)

g.bfs(2)