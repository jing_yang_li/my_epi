
increment_by_i = [lambda x: x + i for i in range(10)]
print(increment_by_i[1](1))

def create_increment_function(x):
    return lambda y: y+x

increment_by_i = [create_increment_function(i) for i in range(10)]
print(increment_by_i[3](4))

# class person:
#     def __init__(self):
#         self.age = 12
#
# def add(a):
#     a.age = 11
#     print("inside ", a.age)
#
# a = person()
# print(a.age)
# add(a)
# print("outside ", a.age)