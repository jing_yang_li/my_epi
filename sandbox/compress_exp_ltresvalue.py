import pandas as pd

file="/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/EXP_LTResValue.csv"
df = pd.read_csv(file)
print(df.head())

cols_to_include = [
    "Res_Name",
    "LT_Iteration",
    "Begin_Year",
    "Capacity"
]
df = df[cols_to_include]
print(df)

# df.to_csv(
#     "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/EXP_LTResValue_partial.csv",
#     index=False
# )

df.to_parquet(
    "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/EXP_LTResValue_partial.par",
    compression='gzip'
)