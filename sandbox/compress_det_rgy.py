import pandas as pd

file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/DET_ResourceGroupYear.csv"
df = pd.read_csv(file)
print(df.head())

cols_to_include = [
    "Output",
    "Name",
    "Capability",
    "Time_Period"
]
df = df[cols_to_include]
print(df)

# df.to_csv(
#     "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/DET_ResourceGroupYear_partial.csv",
#     index=False
# )

df.to_parquet(
    "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/DET_ResourceGroupYear_partial.par",
    compression='gzip'
)