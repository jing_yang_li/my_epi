
import pandas as pd

# EIC
# file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/EIC/wacc_rate_contracted.csv"
file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/EIC/wacc_rate_merchant.csv"
df = pd.read_csv(file)
print(df)

df1 = df[df['type']=='Wind']
# df1 = df[df['type']=='Solar']

# for 'Strg_2Hr' and 'Strg_4Hr'
# df1 = df[df['type']=='Storage']

# for 'CCGT'
# df1 = df[df['type']=='CC']

# df1 = df[df['type']=='CT']

print(df1)
for i in range(2020, 2051):
    value = df1[f'{i}'].values[0]
    # print(round(value, 7))
    print('{0:.7f}'.format(value)+',')