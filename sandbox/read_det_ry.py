import pandas as pd

file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/DET_ResourceYear.csv"
df = pd.read_csv(file)
print(df.head())

cols_to_include = [
    "Zone",
    "Name",
    "Primary_Fuel",
    "Nameplate_Capacity",
    "Peak_Credit",
    "Time_Period",
    "Energy_Revenue",
    "Total_Fuel_Cost",
    "Variable_OM_Cost",
    "Total_Emission_Cost",
    "Startup_Cost",
    "Storage_Charging_Cost",
    "Generation_MWh",
    "Fixed_Cost_Aux1",
    "Fixed_Cost_Base"
]
df = df[cols_to_include]
print(df)

df.to_csv(
    "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/WECC/csv/DET_ResourceYear_partial.csv",
    index=False
)