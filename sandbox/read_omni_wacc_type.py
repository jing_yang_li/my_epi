import pandas as pd

# EIC
file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/EIC/lookup_master.csv"

df = pd.read_csv(file)
aera_pool_mapping = {}
for ind, row in df.iterrows():
    # print(f"[{row['Area Number']}, \"{row['Area Name']}\", \"{row['Short Area Name']}\", \"{row['Pool Name']}\", \"{row['Pool ID']}\"],")
    aera_pool_mapping[row['Consolidation_Area']] = row['Pool ID']

# EIC
file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/EIC/wacc_type.csv"
df = pd.read_csv(file)
print(df)

for ind, row in df.iterrows():
    print("[")
    print(f"\t\"{aera_pool_mapping[row['Zone']]}\",")
    print(f"\t\"{row['Area']}\",")
    print(f"\t\"{row['CCGT']}\",")
    print(f"\t\"{row['CT']}\",")
    print(f"\t\"{row['Solar']}\",")
    print(f"\t\"{row['Wind']}\",")
    print(f"\t\"{row['Strg_2Hr']}\",")
    print(f"\t\"{row['Strg_4Hr']}\",")
    print(f"\t\"{row['Strg_8Hr']}\",")
    print("],")