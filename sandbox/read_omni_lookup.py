import pandas as pd

# EIC
file = "/Users/jli/Documents/Projects/OMNI/data/excel_sheets/EIC/lookup_master.csv"

df = pd.read_csv(file)
print(df)

for ind, row in df.iterrows():
    print(f"[{row['Area Number']}, \"{row['Consolidation_Area']}\", \"{row['Short Area Name']}\", \"{row['Pool Name']}\", \"{row['Pool ID']}\"],")