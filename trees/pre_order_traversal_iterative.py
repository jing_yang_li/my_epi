# A class to store a binary tree node
class Node:
    def __init__(self, key=None, left=None, right=None):
        self.key = key
        self.left = left
        self.right = right


# Function to print all nodes of a given level from left to right
def pre_order(root):

    # base case
    if root is None:
        return

    s = []   # as a stack
    s.append(root)
    while s:
        node = s.pop()   # pop up the last item
        print(node.key)
        if node.right:
            s.append(node.right)
        if node.left:
            s.append(node.left)


if __name__ == '__main__':
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(4)
    root.right.left = Node(5)
    root.right.right = Node(6)
    root.right.left.left = Node(7)
    root.right.left.right = Node(8)
    pre_order(root)
