import pandas as pd
import numpy as np
import tensorflow as tf
import collections
from matplotlib import pyplot as plt

# Add some convenience functions to Pandas DataFrame.
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format

tf.compat.v1.disable_eager_execution()

# Download MovieLens data.
print("Downloading movielens data...")
from urllib.request import urlretrieve
import zipfile

# retrieve the file from remote and save as a local file
# urlretrieve("http://files.grouplens.org/datasets/movielens/ml-100k.zip", "movielens.zip")
zip_ref = zipfile.ZipFile('movielens.zip', "r")
zip_ref.extractall()
print("Done. Dataset contains:")
print(zip_ref.read('ml-100k/u.info'))

# Load each data set (users, movies, and ratings).
users_cols = ['user_id', 'age', 'sex', 'occupation', 'zip_code']
users = pd.read_csv(
    'ml-100k/u.user', sep='|', names=users_cols, encoding='latin-1')
print(len(users))

ratings_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
ratings = pd.read_csv(
    'ml-100k/u.data', sep='\t', names=ratings_cols, encoding='latin-1')
print(len(ratings))

# The movies file contains a binary feature for each genre.
genre_cols = [
    "genre_unknown", "Action", "Adventure", "Animation", "Children", "Comedy",
    "Crime", "Documentary", "Drama", "Fantasy", "Film-Noir", "Horror",
    "Musical", "Mystery", "Romance", "Sci-Fi", "Thriller", "War", "Western"
]
movies_cols = [
                  'movie_id', 'title', 'release_date', "video_release_date", "imdb_url"
              ] + genre_cols
movies = pd.read_csv(
    'ml-100k/u.item', sep='|', names=movies_cols, encoding='latin-1')
print(movies["release_date"])

# Since the ids start at 1, we shift them to start at 0.
users["user_id"] = users["user_id"].apply(lambda x: str(x-1))
movies["movie_id"] = movies["movie_id"].apply(lambda x: str(x-1))
movies["year"] = movies['release_date'].apply(lambda x: str(x).split('-')[-1])  # extract year
ratings["movie_id"] = ratings["movie_id"].apply(lambda x: str(x-1))
ratings["user_id"] = ratings["user_id"].apply(lambda x: str(x-1))
ratings["rating"] = ratings["rating"].apply(lambda x: float(x))

# Compute the number of movies to which a genre is assigned.
genre_occurences = movies[genre_cols].sum().to_dict()
print(genre_occurences)

# Since some movies can belong to more than one genre, we create different
# 'genre' columns as follows:
# - all_genres: all the active genres of the movie.
# - genre: randomly sampled from the active genres.
def mark_genres(movies, genres):

    # pick a random from the active genres for a movie
    def get_random_genre(gs):
        active = [genre for genre, g in zip(genres, gs) if g==1]
        if len(active) == 0:
            return 'Other'
        return np.random.choice(active)

    # concatenate all genres that a movie belongs to
    def get_all_genres(gs):
        active = [genre for genre, g in zip(genres, gs) if g==1]
        if len(active) == 0:
            return 'Other'
        return '-'.join(active)

    movies['genre'] = [
        get_random_genre(gs) for gs in zip(*[movies[genre] for genre in genres])]
    movies['all_genres'] = [
        get_all_genres(gs) for gs in zip(*[movies[genre] for genre in genres])]

mark_genres(movies, genre_cols)
print(movies["all_genres"])
print(movies["genre"])

# print("n of genres ", len(genre_cols))
# # print(([movies[genre] for genre in genre_cols]))
# print(tuple(zip(*[movies[genre] for genre in genre_cols])))
#
# my_list = [1, 2, 3]
# print(my_list)
# print(*my_list)
# print([0, *my_list, 4])

# Create one merged DataFrame containing all the movielens data.
movielens = ratings.merge(movies, on='movie_id').merge(users, on='user_id')
# print(movielens.columns)

# Utility to split the data into training and test sets.
def split_dataframe(df, holdout_fraction=0.1):
    """Splits a DataFrame into training and test sets.
    Args:
      df: a dataframe.
      holdout_fraction: fraction of dataframe rows to use in the test set.
    Returns:
      train: dataframe for training
      test: dataframe for testing
    """
    test = df.sample(frac=holdout_fraction, replace=False)
    train = df[~df.index.isin(test.index)]
    return train, test

# explore
# print(users.describe())

# We can also print some basic statistics describing the categorical user features
# print(users.describe(include=[object]))


def flatten_cols(df):
    df.columns = [' '.join(col).strip() for col in df.columns.values]
    return df

pd.DataFrame.flatten_cols = flatten_cols

#  look at the distribution of ratings per user
users_ratings = (
    ratings
        .groupby('user_id', as_index=False)
        .agg({'rating': ['count', 'mean']})
        .flatten_cols()
        .merge(users, on='user_id')
)

print(users_ratings)

# It is also useful to look at information about the movies and their ratings.
movies_ratings = movies.merge(
    ratings
        .groupby('movie_id', as_index=False)
        .agg({'rating': ['count', 'mean']})
        .flatten_cols(),
    on='movie_id')
# print(movies_ratings)

# print((movies_ratings[['title', 'rating count', 'rating mean']]
#        .sort_values('rating count', ascending=False)
#        .head(10))
#       )

# print((movies_ratings[['title', 'rating count', 'rating mean']]
#        .mask('rating count', lambda x: x > 20)
#        .sort_values('rating mean', ascending=False)
#        .head(10))
#       )

# a function that maps from our ratings DataFrame to a tf.SparseTensor.
def build_rating_sparse_tensor(ratings_df):
    """
    Args:
      ratings_df: a pd.DataFrame with `user_id`, `movie_id` and `rating` columns.
    Returns:
      a tf.SparseTensor representing the ratings matrix.
    """
    indices = ratings_df[['user_id', 'movie_id']].values
    values = ratings_df['rating'].values
    return tf.SparseTensor(
        indices=indices,
        values=values,
        dense_shape=[users.shape[0], movies.shape[0]])

# print(ratings.columns)
sparse_ratings  =  build_rating_sparse_tensor(ratings)
print(sparse_ratings.indices.shape[-1])

#  a TensorFlow function that takes a sparse rating matrix  𝐴  and the two embedding matrices  𝑈,𝑉
#  and returns the mean squared error MSE(A, UV.T)
def sparse_mean_square_error_1(sparse_ratings, user_embeddings, movie_embeddings):
    """
    Args:
      sparse_ratings: A SparseTensor rating matrix, of dense_shape [N, M]
      user_embeddings: A dense Tensor U of shape [N, k] where k is the embedding
        dimension, such that U_i is the embedding of user i.
      movie_embeddings: A dense Tensor V of shape [M, k] where k is the embedding
        dimension, such that V_j is the embedding of movie j.
    Returns:
      A scalar Tensor representing the MSE between the true ratings and the
        model's predictions.
    """
    predictions = tf.gather_nd(
        tf.matmul(user_embeddings, movie_embeddings, transpose_b=True),
        sparse_ratings.indices)   # gather_nd gets slices of a matrix at indices
    loss = tf.losses.mean_squared_error(sparse_ratings.values, predictions)
    return loss

# the above approach is to compute the full prediction matrix, then gather the entries corresponding to the observed pairs
# if the number of users and movies are too big, this approach is infeasible

# Another approach (given in the alternate solution below) is to only gather the embeddings of the observed pairs,
# then compute their dot products.
def sparse_mean_square_error(sparse_ratings, user_embeddings, movie_embeddings):
    """
    Args:
      sparse_ratings: A SparseTensor rating matrix, of dense_shape [N, M]
      user_embeddings: A dense Tensor U of shape [N, k] where k is the embedding
        dimension, such that U_i is the embedding of user i.
      movie_embeddings: A dense Tensor V of shape [M, k] where k is the embedding
        dimension, such that V_j is the embedding of movie j.
    Returns:
      A scalar Tensor representing the MSE between the true ratings and the
        model's predictions.
    """
    predictions = tf.reduce_sum(
        tf.gather(user_embeddings, sparse_ratings.indices[:, 0]) *
        tf.gather(movie_embeddings, sparse_ratings.indices[:, 1]),
        axis=1)
    loss = tf.losses.mean_squared_error(sparse_ratings.values, predictions)
    return loss

# Training a Matrix Factorization model
# Collaborative Filtering Model
'''
This is a simple class to train a matrix factorization model using stochastic gradient descent.

The class constructor takes
- the user embeddings U (a tf.Variable).
- the movie embeddings V, (a tf.Variable).
- a loss to optimize (a tf.Tensor).
- an optional list of metrics dictionaries, each mapping a string (the name of the metric) to a tensor. 
These are evaluated and plotted during training (e.g. training error and test error).
'''

class CFModel(object):
    """Simple class that represents a collaborative filtering model"""
    def __init__(self, embedding_vars, loss, metrics=None):
        """Initializes a CFModel.
        Args:
          embedding_vars: A dictionary of tf.Variables.
          loss: A float Tensor. The loss to optimize.
          metrics: optional list of dictionaries of Tensors. The metrics in each
            dictionary will be plotted in a separate figure during training.
        """
        self._embedding_vars = embedding_vars
        self._loss = loss
        self._metrics = metrics
        self._embeddings = {k: None for k in embedding_vars}
        self._session = None

    @property
    def embeddings(self):
        """The embeddings dictionary."""
        return self._embeddings

    def train(self, num_iterations=100, learning_rate=1.0, plot_results=True,
              optimizer=tf.compat.v1.train.GradientDescentOptimizer):
        """Trains the model.
        Args:
          iterations: number of iterations to run.
          learning_rate: optimizer learning rate.
          plot_results: whether to plot the results at the end of training.
          optimizer: the optimizer to use. Default to GradientDescentOptimizer.
        Returns:
          The metrics dictionary evaluated at the last iteration.
        """
        with self._loss.graph.as_default():
            opt = optimizer(learning_rate)
            train_op = opt.minimize(self._loss)
            local_init_op = tf.group(
                tf.compat.v1.variables_initializer(opt.variables()),
                tf.compat.v1.local_variables_initializer())
            if self._session is None:
                self._session = tf.compat.v1.Session()
                with self._session.as_default():
                    self._session.run(tf.compat.v1.global_variables_initializer())
                    self._session.run(tf.compat.v1.tables_initializer())
                    tf.compat.v1.train.start_queue_runners()

        with self._session.as_default():
            local_init_op.run()
            iterations = []
            metrics = self._metrics or ({},)
            metrics_vals = [collections.defaultdict(list) for _ in self._metrics]

            # Train and append results.
            for i in range(num_iterations + 1):
                _, results = self._session.run((train_op, metrics))
                if (i % 10 == 0) or i == num_iterations:
                    print("\r iteration %d: " % i + ", ".join(
                        ["%s=%f" % (k, v) for r in results for k, v in r.items()]),
                          end='')
                    iterations.append(i)
                    for metric_val, result in zip(metrics_vals, results):
                        for k, v in result.items():
                            metric_val[k].append(v)

            for k, v in self._embedding_vars.items():
                self._embeddings[k] = v.eval()

            if plot_results:
                # Plot the metrics.
                num_subplots = len(metrics)+1
                fig = plt.figure()
                fig.set_size_inches(num_subplots*10, 8)
                for i, metric_vals in enumerate(metrics_vals):
                    ax = fig.add_subplot(1, num_subplots, i+1)
                    for k, v in metric_vals.items():
                        ax.plot(iterations, v, label=k)
                    ax.set_xlim([1, num_iterations])
                    ax.legend()
                plt.show()
            return results

def build_model(ratings, embedding_dim=3, init_stddev=1.):
    """
    Args:
      ratings: a DataFrame of the ratings
      embedding_dim: the dimension of the embedding vectors.
      init_stddev: float, the standard deviation of the random initial embeddings.
    Returns:
      model: a CFModel.
    """
    # Split the ratings DataFrame into train and test.
    train_ratings, test_ratings = split_dataframe(ratings)
    # SparseTensor representation of the train and test datasets.
    # ========================= Complete this section ============================
    A_train = build_rating_sparse_tensor(train_ratings)
    A_test = build_rating_sparse_tensor(test_ratings)
    # ============================================================================
    # Initialize the embeddings using a normal distribution.
    U = tf.Variable(tf.random.normal(
        [A_train.dense_shape[0], embedding_dim], stddev=init_stddev))
    V = tf.Variable(tf.random.normal(
        [A_train.dense_shape[1], embedding_dim], stddev=init_stddev))
    # ========================= Complete this section ============================
    train_loss = sparse_mean_square_error(A_train, U, V)
    test_loss = sparse_mean_square_error(A_test, U, V)
    # ============================================================================
    metrics = {
        'train_error': train_loss,
        'test_error': test_loss
    }
    embeddings = {
        "user_id": U,
        "movie_id": V
    }
    return CFModel(embeddings, train_loss, [metrics])

# train the model
# model = build_model(ratings, embedding_dim=30, init_stddev=0.5)
# model.train(num_iterations=1000, learning_rate=10.)

# inspecting the embeddings
'''
computing your recommendations
looking at the nearest neighbors of some movies,
looking at the norms of the movie embeddings,
visualizing the embedding in a projected embedding space.
'''

# Write a function that computes the scores of the candidates
# for a given user embedding and item embedding, compute the item score, the similarity
DOT = 'dot'
COSINE = 'cosine'
def compute_scores(query_embedding, item_embeddings, measure=DOT):
    """Computes the scores of the candidates given a query.
    Args:
      query_embedding: a vector of shape [k], representing the query embedding.
      item_embeddings: a matrix of shape [N, k], such that row i is the embedding
        of item i.
      measure: a string specifying the similarity measure to be used. Can be
        either DOT or COSINE.
    Returns:
      scores: a vector of shape [N], such that scores[i] is the score of item i.
    """
    u = query_embedding
    V = item_embeddings
    if measure == COSINE:
        V = V / np.linalg.norm(V, axis=1, keepdims=True)
        u = u / np.linalg.norm(u)
    scores = u.dot(V.T)
    return scores

USER_RATINGS = True

# with this function, we can compute recommendations
# @title User recommendations and nearest neighbors (run this cell)
def user_recommendations(model, measure=DOT, exclude_rated=False, k=6):
    if USER_RATINGS:
        scores = compute_scores(
            model.embeddings["user_id"][942], model.embeddings["movie_id"], measure)
        score_key = measure + ' score'
        df = pd.DataFrame({
            score_key: list(scores),
            'movie_id': movies['movie_id'],
            'titles': movies['title'],
            'genres': movies['all_genres'],
        })
        if exclude_rated:
            # remove movies that are already rated
            rated_movies = ratings[ratings.user_id == "943"]["movie_id"].values
            df = df[df.movie_id.apply(lambda movie_id: movie_id not in rated_movies)]
        # display.display(df.sort_values([score_key], ascending=False).head(k))
        print(df.sort_values([score_key], ascending=False).head(k))

# user_recommendations(model, measure=COSINE, k=5)

# --------------------------------------
# Regularization In Matrix Factorization

# In the previous section, our loss was defined as the mean squared error on the observed part of the rating matrix.
# As discussed in the lecture, this can be problematic as the model does not learn how to place the embeddings of
# irrelevant movies. This phenomenon is known as folding.
#
# We will add regularization terms that will address this issue. We will use two types of regularization:
# 1. Regularization of the model parameters.
# 2. A global prior that pushes the prediction of any pair towards zero, called the gravity term.

# Build a regularized Matrix Factorization model and train it
def gravity(U, V):
    """Creates a gravity loss given two embedding matrices."""
    print("U shape ", U.shape)
    print("V shape ", V.shape)
    return 1. / (U.shape[0] * V.shape[0]) * tf.reduce_sum(
        tf.matmul(U, U, transpose_a=True) * tf.matmul(V, V, transpose_a=True))

def build_regularized_model(
        ratings, embedding_dim=3, regularization_coeff=.1, gravity_coeff=1.,
        init_stddev=0.1):
    """
    Args:
      ratings: the DataFrame of movie ratings.
      embedding_dim: The dimension of the embedding space.
      regularization_coeff: The regularization coefficient lambda.
      gravity_coeff: The gravity regularization coefficient lambda_g.
    Returns:
      A CFModel object that uses a regularized loss.
    """
    # Split the ratings DataFrame into train and test.
    train_ratings, test_ratings = split_dataframe(ratings)
    # SparseTensor representation of the train and test datasets.
    A_train = build_rating_sparse_tensor(train_ratings)
    A_test = build_rating_sparse_tensor(test_ratings)
    U = tf.Variable(tf.compat.v1.random_normal(
        [A_train.dense_shape[0], embedding_dim], stddev=init_stddev))
    V = tf.Variable(tf.compat.v1.random_normal(
        [A_train.dense_shape[1], embedding_dim], stddev=init_stddev))

    error_train = sparse_mean_square_error(A_train, U, V)
    error_test = sparse_mean_square_error(A_test, U, V)
    gravity_loss = gravity_coeff * gravity(U, V)
    regularization_loss = regularization_coeff * (
            tf.reduce_sum(U*U)/U.shape[0] + tf.reduce_sum(V*V)/V.shape[0])
    total_loss = error_train + regularization_loss + gravity_loss
    losses = {
        'train_error_observed': error_train,
        'test_error_observed': error_test,
    }
    loss_components = {
        'observed_loss': error_train,
        'regularization_loss': regularization_loss,
        'gravity_loss': gravity_loss,
    }
    embeddings = {"user_id": U, "movie_id": V}

    return CFModel(embeddings, total_loss, [losses, loss_components])

# It is now time to train the regularized model! You can try different values of the regularization coefficients,
# and different embedding dimensions.
reg_model = build_regularized_model(
    ratings, regularization_coeff=0.1, gravity_coeff=1.0, embedding_dim=35,
    init_stddev=.05)
# train the model
# reg_model.train(num_iterations=2000, learning_rate=20.)

#Observe that adding the regularization terms results in a higher MSE, both on the training and test set. However,
# as we will see, the quality of the recommendations improves. This highlights a tension between fitting the observed
# data and minimizing the regularization terms. Fitting the observed data often emphasizes learning high similarity
# (between items with many interactions), but a good embedding representation also requires learning low similarity
# (between items with few or no interactions).
# user_recommendations(reg_model, DOT, exclude_rated=True, k=10)

'''
Note that while the scale of the problem is small enough to allow efficient training using SGD, many practical problems 
need to be trained using more specialized algorithms such as Alternating Least Squares 
(see tf.contrib.factorization.WALSMatrixFactorization for a TF implementation).
'''

#==================== =========================
# softmax model
#---------------
# train a simple softmax model that predicts whether a given user has rated a movie.

# The model will take as input a feature vector  𝑥  representing the list of movies the user has rated. We start from
# the ratings DataFrame, which we group by user_id.

rated_movies = (ratings[["user_id", "movie_id"]]
                .groupby("user_id", as_index=False)
                .aggregate(lambda x: list(x)))
print(rated_movies.head())

'''
We then create a function that generates an example batch, such that each example contains the following features:

movie_id: A tensor of strings of the movie ids that the user rated.
genre: A tensor of strings of the genres of those movies
year: A tensor of strings of the release year.
'''
#@title Batch generation code (run this cell)
years_dict = {
    movie: year for movie, year in zip(movies["movie_id"], movies["year"])
}
genres_dict = {
    movie: genres.split('-')
    for movie, genres in zip(movies["movie_id"], movies["all_genres"])
}


def make_batch(ratings, batch_size):
    """Creates a batch of examples.
    Args:
      ratings: A DataFrame of ratings such that examples["movie_id"] is a list of
        movies rated by a user.
      batch_size: The batch size.
    """
    def pad(x, fill):
        return pd.DataFrame.from_dict(x).fillna(fill).values

    movie = []  # each item in the list is a list of movies rated by a user
    year = []
    genre = []
    label = []
    for movie_ids in ratings["movie_id"].values:
        movie.append(movie_ids)
        genre.append([x for movie_id in movie_ids for x in genres_dict[movie_id]])
        year.append([years_dict[movie_id] for movie_id in movie_ids])
        label.append([int(movie_id) for movie_id in movie_ids])
    features = {
        "movie_id": pad(movie, ""),
        "year": pad(year, ""),
        "genre": pad(genre, ""),
        "label": pad(label, -1)
    }
    # https://stackoverflow.com/questions/53514495/what-does-batch-repeat-and-shuffle-do-with-tensorflow-dataset
    batch = (
        tf.compat.v1.data.Dataset.from_tensor_slices(features)
            .shuffle(1000)
            .repeat()
            .batch(batch_size)
            .make_one_shot_iterator()
            .get_next()
    )

    return batch


def select_random(x):
    """Selectes a random elements from each row of x."""
    def to_float(x):
        return tf.cast(x, tf.float32)
    def to_int(x):
        return tf.cast(x, tf.int64)
    batch_size = tf.shape(x)[0]
    rn = tf.range(batch_size)
    nnz = to_float(tf.compat.v1.count_nonzero(x >= 0, axis=1))
    rnd = tf.compat.v1.random_uniform([batch_size])
    ids = tf.stack([to_int(rn), to_int(nnz * rnd)], axis=1)
    return to_int(tf.gather_nd(x, ids))

def softmax_loss(user_embeddings, movie_embeddings, labels):
    """Returns the cross-entropy loss of the softmax model.
    Args:
      user_embeddings: A tensor of shape [batch_size, embedding_dim].
      movie_embeddings: A tensor of shape [num_movies, embedding_dim].
      labels: A tensor of [batch_size], such that labels[i] is the target label
        for example i.
    Returns:
      The mean cross-entropy loss.
    """
    # Verify that the embddings have compatible dimensions
    user_emb_dim = user_embeddings.shape[1].value
    movie_emb_dim = movie_embeddings.shape[1].value
    if user_emb_dim != movie_emb_dim:
        raise ValueError(
            "The user embedding dimension %d should match the movie embedding "
            "dimension % d" % (user_emb_dim, movie_emb_dim))

    logits = tf.matmul(user_embeddings, movie_embeddings, transpose_b=True)
    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logits, labels=labels))
    return loss


train_batch = make_batch(rated_movies, 200)
print(train_batch["label"])
print(len(movies))

ratings = rated_movies
movie = []
year = []
genre = []
label = []
for movie_ids in ratings["movie_id"].values:
    # print(len(movie_ids))
    movie.append(movie_ids)
    genre.append([x for movie_id in movie_ids for x in genres_dict[movie_id]])
    year.append([years_dict[movie_id] for movie_id in movie_ids])
    label.append([int(movie_id) for movie_id in movie_ids])

features = {
    "movie_id": movie,
    "year": year,
    "genre": genre,
    "label": label
}
# dataset = (tf.data.Dataset.from_tensor_slices(features)
#             .shuffle(1000)
#             .repeat()
#             .batch(300)
#             .make_one_shot_iterator()
#            .get_next()
#            )

