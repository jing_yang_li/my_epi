import pandas as pd
df = pd.DataFrame({'col1': [1, 2], 'col2': [3, 4]})
print(df.shape[-1])

l = (1,2,4)
print(l[-1])

import tensorflow as tf
from math import log, exp
print(log(exp(2) / (exp(2) + exp(-5) + exp(0.5) + exp(-0.1))))

print(log(exp(1.4) / (exp(0) + exp(0) + exp(1.9) + exp(1.4))))

import tensorflow as tf

logits = tf.constant([[2., -5., .5, -.1],
                      [0., 0., 1.9, 1.4],
                      [-100., 100., -100., -100.]])
labels = tf.constant([0, 3, 1])
# labels = tf.constant([[0,1], [1, 3], [1,2]])
loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=logits)
print(loss.numpy())

dataset = tf.data.Dataset.from_tensor_slices([1, 2, 3])
for element in dataset:
    print(element)

