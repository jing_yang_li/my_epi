from tdigest import TDigest
from numpy.random import random

digest = TDigest()

# https://pypi.org/project/tdigest/

# Update the digest sequentially
for x in range(5000):
    digest.update(random())

print(digest.percentile(15))
# about 0.15, as 0.15 is the 15th percentile of the Uniform(0,1) distribution

# Update the digest in batches
another_digest = TDigest()
another_digest.batch_update(random(5000))
print(another_digest.percentile(15))

# Sum two digests to create a new digest
sum_digest = digest + another_digest
print(sum_digest.percentile(30)) # about 0.3

digest.batch_update(random(5000))
print(digest.percentile(30)) # about 0.3

## To dict or serializing a digest with JSON

# You can use the to_dict() method to turn a TDigest object into a standard Python dictionary.
#
# digest = TDigest()
# digest.update(1)
# digest.update(2)
# digest.update(3)
# print(digest.to_dict())
