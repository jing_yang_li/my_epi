
# check if the colored graph is safe or not
def isSafe(graph, colors):
    n_vertices = len(graph)
    # check for every edge
    for i in range(n_vertices):
        for j in range(i + 1, n_vertices):
            if (graph[i][j] and colors[j] == colors[i]):
                return False
    return True

def m_coloring(graph, m, i, colors):

    # if current index reached end
    if i == len(graph):
        # if coloring is safe
        if (isSafe(graph, colors)):
            print(colors)
            return True
        return False

    # assign each color from 1 to m
    for j in range(1, m+1):
        colors[i] = j
        ## Recur of the rest vertices
        if m_coloring(graph, m, i+1, colors):
            return True
        colors[i] = 0

# driver code
graph = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [1, 1, 0, 1],
    [1, 0, 1, 0]
]
m = 3
colors = [0] * len(graph)
if (m_coloring(graph, m, 0, colors)):
    print(colors)
# [1, 2, 3, 2]

p = [81.01, 82.55, 84.12, 85.72, 87.35]
print([i*1.019 for i in p])
# $85.72
# $87.35
# $89.01
# $90.70
# $92.42
# $94.18
# $95.97
# $97.79
# $99.65

print(79.50*1.019)
