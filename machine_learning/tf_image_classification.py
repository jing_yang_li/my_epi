import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# use the fashion MNIST dataset
fashion_mnist = tf.keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# The images are 28x28 NumPy arrays, with pixel values ranging from 0 to 255. The labels are an array of integers,
# ranging from 0 to 9. These correspond to the class of clothing the image represents
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

print(train_images.shape)

# pre-processing the data - sacle to [0, 1]
# Scale these values to a range of 0 to 1 before feeding them to the neural network model. To do so, divide the values
# by 255. It's important that the training set and the testing set be preprocessed in the same way:
train_images = train_images / 255.0
test_images = test_images / 255.0

# build the model

# setup layers
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(10),
    tf.keras.layers.Softmax()
])
# The first layer in this network, tf.keras.layers.Flatten, transforms the format of the images from a
# two-dimensional array (of 28 by 28 pixels) to a one-dimensional array (of 28 * 28 = 784 pixels).
# The first Dense layer has 128 nodes (or neurons).
# The second (and last) layer returns a logits array with length of 10.

# compile model
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

# train the model
model.fit(train_images, train_labels, epochs=10)

# evaluate accuracy
test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('\nTest accuracy:', test_acc)

predictions = model.predict(test_images)
print(predictions[0])

# A prediction is an array of 10 numbers. They represent the model's "confidence" that the image corresponds to
# each of the 10 different articles of clothing. You can see which label has the highest confidence value:
print(np.argmax(predictions[0]))

# So, the model is most confident that this image is an ankle boot, or class_names[9].
# Examining the test label shows that this classification is correct:
print(test_labels[0])