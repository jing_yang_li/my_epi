import numpy as np
import pandas as pd

class LinearRegression:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.n_samples = x.shape[0]
        self.n_features = x.shape[1]

        # mean and std of variables in x
        self.mu = 0
        self.sigma = 1

        # learning rate
        self.alpha = 0.005

        # parameters for variables in x
        self.theta = None

    # normalize the variables in x
    def normalize_feature(self, x):
        mu = np.mean(x, axis=0)
        sigma = np.std(x, axis=0)
        x_norm = (x - mu)/sigma
        return x_norm, mu, sigma

    # add x0 with value of 1s to x
    def add_ones(self, x, m):
        x = np.c_[np.ones((m, 1)), x]
        return x

    def fit(self):
        x_norm, self.mu, self.sigma = self.normalize_feature(self.x)
        x = self.add_ones(x_norm, self.n_samples)

        # theta parameter for every input variable. we have n+1 input variable including X_0 (column of ones)
        theta = np.random.rand(self.n_features + 1)

        self.theta = self.gradient_descent(x, y, self.n_samples, theta, self.alpha)

    def gradient_descent(self, x, y, m, theta, alpha):
        run = True

        pre_cost = float("inf")

        while run:
            predictions = np.dot(x, theta)
            error = predictions - y

            cur_cost = 1/(2*m) * np.dot(error.T, error)

            # derivative
            d_w = alpha * (1/m) * np.dot(x.T, error)
            theta = theta - d_w

            if pre_cost - cur_cost < 1e-9:
                run = False

            pre_cost = cur_cost

        return theta

    def predict(self, x):
        x_norm = (x - self.mu)/self.sigma
        n_samples = x_norm.shape[0]
        x = self.add_ones(x_norm, n_samples)
        predictions = np.dot(x, self.theta)
        print(predictions)

df = pd.read_csv('https://raw.githubusercontent.com/satishgunjal/datasets/master/multivariate_housing_prices_in_portlans_oregon.csv')

x = np.array(df.iloc[:, 0:2])
y = np.array(df.iloc[:, 2])

# train
lr = LinearRegression(x, y)
lr.fit()

# predict
lr.predict(x)