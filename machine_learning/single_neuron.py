import numpy as np

# https://stackabuse.com/creating-a-neural-network-from-scratch-in-python/
# https://www.mldawn.com/binary-classification-from-scratch-using-numpy/

def sigmoid(z):
    return 1/(1 + np.exp(-z))

def sigmoid_derivative(z):
    return sigmoid(z) * (1 - sigmoid(z))

def cross_entropy(y_hat, y):
    return y * np.log(y_hat) - (1-y) * np.log(1 - y_hat)

def cross_entropy_derivative(y_hat, y):
    return -y / y_hat + (1-y) / (1 - y_hat)

def train(X, y, weights, bias):

    for epoch in range(100):

        # forward propagation
        z = np.dot(X, weights) + bias
        y_hat = sigmoid(z)

        # backpropagation
        error = cross_entropy(y_hat, y)

        dw = np.dot(X.T, cross_entropy_derivative(y_hat, y) * sigmoid_derivative(z))
        dw_0 = cross_entropy_derivative(y_hat, y) * sigmoid_derivative(z)  # for bias

        weights -= dw
        bias -= np.sum(dw_0)
        print(np.mean(error))



# create training data
# 5 records
# each record has 3 features
feature_set = np.array([[0,1,0],[0,0,1],[1,0,0],[1,1,0],[1,1,1]])
labels = np.array([[1,0,0,1,1]])
labels = labels.reshape(5,1)

# b = np.array([[2,3,4,5,6]])
# b = b.reshape(5,1)
# print(labels * b)


# define parameters
np.random.seed(42)
weights = np.random.rand(3, 1)
bias = np.random.rand(1)
lr = 0.05


train(feature_set, labels, weights, bias)