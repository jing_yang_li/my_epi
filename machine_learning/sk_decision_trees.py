from sklearn import datasets
from sklearn.tree import DecisionTreeClassifier

iris = datasets.load_iris()
x = iris['data']
x = x[:, 2:]
y = iris['target']

tree_clf = DecisionTreeClassifier(max_depth=2)
tree_clf.fit(x, y)

pred = tree_clf.predict([[5, 1.5]])
print(pred)