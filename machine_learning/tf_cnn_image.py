from tensorflow.keras import layers, models
import tensorflow as tf

def classify_trucks():
    model = models.Sequential()
    model.add(layers.Conv2D(16, (3, 3), activation='relu', input_shape=(224,224, 3)), kernel_initializer="he_normal")
    model.add(layers.MaxPooling2D((2, 2))) # by default using valid padding

    model.add(layers.Conv2D(32, (3, 3), activation='relu'), kernel_initializer="he_normal")
    model.add(layers.MaxPooling2D((2, 2)))
    
    model.add(layers.Conv2D(64, (3, 3), activation='relu'), kernel_initializer="he_normal")
    model.add(layers.Flatten())
    model.add(layers.Dense(20, activation='relu'), kernel_initializer="he_normal")
    model.add(layers.Dense(1, activation='sigmoid'), kernel_initializer="glorot_normal")

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                  metrics=['binary_accuracy'])

    return model