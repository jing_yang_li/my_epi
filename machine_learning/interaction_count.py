from datetime import date, timedelta
import random
import pandas as pd

def count_users(df):
    groups = df.groupby("user_id")
    count = {}

    user_col = []
    month_col = []
    week_col = []
    day_col = []
    for name, group in groups:
        user_col.append(name)

        latest_date = max(group['date'])
        group['month'] = group['date'] + timedelta(30)
        group['week'] = group['date'] + timedelta(7)
        group['day'] = group['date'] + timedelta(1)

        print("total ",  len(group))
        n_month = len(group[group['month']>latest_date])
        n_week = len(group[group['week']>latest_date])
        n_day = len(group[group['day']>latest_date])
        print("month ", n_month)
        print("week ", n_week)
        print("day ", n_day)

        month_col.append(n_month)
        week_col.append(n_week)
        day_col.append(n_day)

    count_df = pd.DataFrame(
        {
            "user_id": user_col,
            "month_count": month_col,
            "week_count": week_col,
            "day_count": day_col
        }
    )

    print(count_df)

random.seed(0)
users = ["u1", "u2", "u3"]
today = date.today()

delta = timedelta(5)
past = today - delta
print(today)
print(past)

# create raw df
n = 20
user_col = [users[random.randint(0, len(users)-1)] for i in range(n)]
dt_col = [today - timedelta(random.randint(0, 40)) for i in range(n)]
print(user_col)
print(dt_col)

records = {
    "user_id": user_col,
    "date": dt_col
}

df = pd.DataFrame(records)

# d = df.groupby("user_id").count()
# print(d)
# count_users(df)

print(df.where(df['user_id']=='u1')['date'])