import pandas as pd

# Utility to split the data into training and test sets
def split_dataframe(df, holdout_fraction=0.1):
    """Splits a DataFrame into training and test sets.
    Args:
        df: a dataframe.
        holdout_fraction: fraction of dataframe rows to use in the test set.
    Returns:
        train: dataframe for training
        test: dataframe for testing
    """
    test = df.sample(frac=holdout_fraction, replace=False)
    train = df[~df.index.isin(test.index)]
    return train, test


from sklearn import datasets
iris = datasets.load_iris()
df = pd.DataFrame(iris['data'])
train, test = split_dataframe(df, holdout_fraction=0.1)
print(len(df), len(train), len(test))
