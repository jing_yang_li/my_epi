from sklearn import datasets

# https://cmdlinetips.com/2021/11/access-datasets-from-scikit-learn/

# ********* TOY Data **************
# To see the list of the “Toy Datasets” in the datasets package
all_data = [data for data in dir(datasets) if data.startswith("load")]
print(all_data)

# Scikit-learn stores each of the dataset in a dictionary like structure.
# We can look at the attributes of iris data set using dir()
iris = datasets.load_iris()
print(dir(iris))

# iris.data or iris['data'] gives the data as a numpy 2D array.
print(iris.data.shape)
print(iris['data'].shape)
# output: (150, 4)

# get feature names
print(iris['feature_names'])
# output: ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']

# get the target group by using iris[‘target’]
print(iris['target'])
# output: 0, 1, or 2

# get the names of the target groups using iris[‘target_names’]
print(iris['target_names'])
# output: ['setosa' 'versicolor' 'virginica']