import random
import math
from collections import defaultdict

def closest_group(centroids, p):
    group = None
    min_dist = float('inf')
    for g, c in centroids.items():
        d = math.sqrt(math.pow(p[0] - c[0], 2) + math.pow(p[1] * c[1], 2))
        if d < min_dist:
            min_dist = d
            group = g
    return group

def update_centroid(points):
    x_mean = sum([p[0] for p in points]) / len(points)
    y_mean = sum([p[1] for p in points]) / len(points)
    return (x_mean, y_mean)

def k_means(samples, k, iterations=20):
    # initialize k centroids
    min_x = min([p[0] for p in samples])
    max_x = max([p[0] for p in samples])
    min_y = min([p[1] for p in samples])
    max_y = max([p[1] for p in samples])
    print(min_x, max_x)
    print(min_y, max_y)

    x_values = random.sample(range(min_x, max_x + 1), k)
    y_values = random.sample(range(min_y, max_y + 1), k)

    centroids = {}
    for i in range(k):
        centroids[f'g{i}'] = (x_values[i], y_values[i])

    print(centroids)
    for _ in range(iterations):
        # assign samples to clusters
        assignments = defaultdict(list)
        for p in samples:
            group = closest_group(centroids, p)
            assignments[group].append(p)

        for k, v in assignments.items():
            # print(k, len(v))
            new_center = update_centroid(v)
            # print(k, update_centroid(v))
            centroids[k] = new_center

    # update centroids
    for k, v in assignments.items():
        print(k, v)


# random.seed(9)
n_points = 20
# x_values = [random.randint(1, 10) for _ in range(n_points)]
x_values = random.sample(range(1, 50), n_points)
# y_values = [random.randint(1, 10) for _ in range(n_points)]
y_values = random.sample(range(1, 30), n_points)
samples = [(x_values[i], y_values[i]) for i in range(n_points)]
print(samples)

k_means(samples, 3)