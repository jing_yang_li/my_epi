import math
from collections import defaultdict

def classfy_a_point(points, p, k=3):

    distances = []

    for c, c_points in points.items():
        for q in c_points:
            d = math.sqrt(p[0]*q[0] + p[1]*q[1])
            distances.append((d, c))

    # sort distances by the first element of the tuple
    distances = sorted(distances, key=lambda x: x[0])
    print(distances)

    # select first k distances
    distances = distances[:k]

    # count points that are close to p in each group
    group_count = defaultdict(int)
    for d in distances:
        group_count[d[1]] += 1

    # find the group with the most points
    print(group_count)
    majority_group = None
    max_count = 0
    for group, count in group_count.items():
        if count > max_count:
            max_count = count
            majority_group = group

    return majority_group


# Dictionary of training points having two keys - 0 and 1
# key 0 have points belong to class 0
# key 1 have points belong to class 1

points = {0:[(1,12),(2,5),(3,6),(3,10),(3.5,8),(2,11),(2,9),(1,7)],
            1:[(5,3),(3,2),(1.5,9),(7,2),(6,1),(3.8,1),(5.6,4),(4,2),(2,5)]}

# testing point p(x,y)
p = (2.5,7)

# Number of neighbours
k = 7

group = classfy_a_point(points, p, k)
print(group)