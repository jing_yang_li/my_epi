# Input: an array of distinct elements and a size
# Output: a random subset of the given size of the array elements
# - all subsets should be equally likely
# - return the result in input array itself

# solution:
# k = 1, generate a random number r, swap a[0] with a[r]
# k > 1, choose one element at random as above, then repeat the same process
# with the n-1 element subarray a[1,n-1], until the random subset occupies the
# slots a[0, k-1], and the remaining elements are in the last n-k slots
# space: O(1)
# time: O(k)

import random

def random_sampling(a, k):
    for i in range(k):
        print(i)
        r = random.randint(i, len(a) - 1)
        a[i], a[r] = a[r], a[i]

a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
random_sampling(a, 1)
print(a)
