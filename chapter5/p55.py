# input: a sorted array
# output: all duplicates being removed, return the number of valid entries

# keep track of the next location where next valid entries will be shifted to
# time complexity O(n)
def delete_duplicate(a):
    if not a:
        return 0

    write_index = 1
    for i in range(1, len(a)):
        if a[write_index - 1] != a[i]:
            a[write_index] = a[i]
            write_index += 1
    return write_index

# to get the new array, a[:write_index]
# there are still entries beyond write_index

a = [2, 3, 5, 5, 7, 11, 11, 11, 13]
print(delete_duplicate(a))
print(a)