# permutation can be specified by an array P
# P[i] represents the location oof the element at i in the permutation
# for example, P = (2,0,1,3) maps
#   the element of location 0 to location 2,
#   the element at location 1 to location 2,
#   the element at location 3 unchanged
# the permutation (2,0,1,3) applied to A = (a,b,c,d) yields the array (b,c,a,d)

# Problem: givenan array A of n elements and a permutation P, apply P to A
# solution:
# swap elements in A based on P and also swap the corresponding values in P
# unitl every P[i] == i

def apply_permutation(p, a):
    for i in range(len(a)):
        while p[i] != i:
            a[p[i]], a[i] = a[i], a[p[i]]
            p[p[i]], p[i] = p[i], p[p[i]]

a = ["a", "b", "c", "d"]
p = [2,0,1,3]

apply_permutation(p, a)
print(a)
