# input: two integers represented by arrays with the most significant digit appearing first,
#        and a negative leading digit denoting a negative integer
# ouput: an integer representing their product
#
# example, n1=[3,6,7], n2=[-1,2,3], result=[-4,5,1,4,1]

def multiply(num1, num2):
    sign = -1 if (num1[0] < 0) ^ (num2[0] < 0) else 1  # ^ XOR
    num1[0], num2[0] = abs(num1[0]), abs(num2[0])

    result = [0] * (len(num1) + len(num2)) # max number of digits

    for i in reversed(range(len(num1))):
        for j in reversed(range(len(num2))):
            result[i + j + 1] += num1[i] * num2[j]
            result[i + j] += result[i + j + 1]//10
            result[i + j + 1] %= 10

    print(result)
    # remove the leading zeroes
    first_non_zero_idx = 0
    non_zero_indices = (i for i, x in enumerate(result) if x != 0)
    first_non_zero_idx = next(non_zero_indices, len(result))
    # for i, x in enumerate(result):
    #     if x != 0:
    #         first_non_zero_idx = i
    #         break
    print(first_non_zero_idx)
    result = result[first_non_zero_idx:]

    return [sign * result[0]] + result[1:]

# O(n*m)

num1 = [-1,2,5]
num2 = [2,3,4]
print(multiply(num1, num2))

l = [3,2,1]
for i, x in enumerate(l):
    print(i, x)
# print(non_zero_idx)

