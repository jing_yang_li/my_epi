import itertools
from typing import Iterator
import random

# input: size k, stream of packets
# output: continuously maintain a uniform random subset of size k of the read packets

def online_sample(stream: Iterator[int], k: int):
    # stores the first k elements
    running_sample = list(itertools.islice(stream, k))

    # have read the first k elements
    num_seen_so_far = k

    for x in stream:
        num_seen_so_far += 1
        idx_to_replace = random.randrange(num_seen_so_far)
        if idx_to_replace < k:
            running_sample[idx_to_replace] = x

    return running_sample


t = [1,2,3,4,5,6,7,8,9]
stream = iter(t)
sample = online_sample(stream, 3)
print(sample)