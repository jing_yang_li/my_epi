# creates uniformly random permutatioons oof [0, 1, 2, ..., n-1]

# solution: use the algorithm in p5.12 with k = n

from typing import List
import random

def random_sampling(a, k):
    for i in range(k):
        print(i)
        r = random.randint(i, len(a) - 1)
        a[i], a[r] = a[r], a[i]

def compute_random_permutation(n: int) -> List[int]:
    permutation = list(range(n))
    random_sampling(permutation, n)
    return permutation

random_permutation = compute_random_permutation(5)
print(random_permutation)