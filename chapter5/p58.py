# input: an array a
# output: rearrange a to have a new array B with property
# b[0] <= b[1] >= b[2] <= b[3] >= b[4] .....

# one solution is to sort a and interleave the botton and top halves

# the desired ordering is very local
# swap a[i] and a[i+1] if i is even and a[i] > a[i+1]
# swap a[i] and a[i+1] if i is odd and a[i] < a[i+1]
# O(n)
def rearrange(a):
    for i in range(len(a)):
        a[i:i+2] = sorted(a[i:i+2], reverse=bool(i % 2))

a = [8, 1, 2, 3, 4, 5, 6, 7]
rearrange(a)
print(a)