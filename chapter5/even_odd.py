# at page.40

# input: array of integers
# output: even entries appear first

def even_odd(a):
    print(a)

    next_even, next_odd = 0, len(a)-1
    while next_even < next_odd:
        if a[next_even] % 2 == 0:
            next_even += 1 # only move the pointer when the value at that location is even
        else:
            a[next_even], a[next_odd] = a[next_odd], a[next_even] #swap
            next_odd -= 1

    print(a)

a = [1, 2, 3, 4, 5]
even_odd(a)

print(100*61000/45000000)
print(100*34157/37000000)
print(100*180000/5000000)
