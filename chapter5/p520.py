
# compute rows in Pascal's triangle

# example
#     1
#    1 1
#   1 2 1
#  1 3 3 1
# 1 4 6 4 1

# features: each entry is the sum of the numbers in the adjacent entries above it

# solution: keep the array left-aligned, first entry at location 0
# jth entry at jth row is 1 if j=0 or j=i
# otherwise it is the sum of (j-1)th and jth entries in (i-1)th row

def generate_pascal_trianglee(n):
    result = [[1] * (i+1) for i in range(n)] # generate an array
    for i in range(n):
        for j in range(1,i):
            result[i][j] = result[i - 1][j-1] + result[i - 1][j]
    return result


n = 3
result = [[1] * (i+1) for i in range(n)]
# result = [[1] * 3 for i in range(n)]
print(result)

print(generate_pascal_trianglee(4))