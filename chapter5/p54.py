# each value in an array represents the maximum you can advance to the next position
# begin at the first position, win by getting to the last position

# example, a = (3, 3, 1, 0, 2, 0, 1)
# 1 step from a[0] to a[1] 1<=3
# 3 steps from a[1] to a[4] 3<=3
# 2 steps from a[4] to a[6] 2<=2

# if a = (3, 2, 0, 0, 2, 0, 1), it is impossible to pass a[3], so can't win

# input: an array
# output: whether it is possible to advance to the last position

def can_reach_end(a):
    furthest_reach_so_far, last_index = 0, len(a) - 1
    i = 0
    while i <= furthest_reach_so_far and furthest_reach_so_far < last_index:
        furthest_reach_so_far = max(furthest_reach_so_far, a[i]+i)
        i += 1

    return furthest_reach_so_far >= last_index

a = [3, 3, 2, 0, 2, 0, 1] # true
a = [3, 2, 0, 0, 2, 0, 1] # false
print(can_reach_end(a))
