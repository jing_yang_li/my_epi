# input: array A, index pivot_index
# output:  an array with elements smaller than A[pivot_index] appear first,
# followed by A[pivot_index], followed by elements greater than A[pivot_index]

def dutch_flag_partition_1(a, pivot_index):
    pivot = a[pivot_index]
    # look for element smaller than pivot and move it to front
    for i in range(len(a)):
        for j in range(i+1, len(a)):
            if a[j] < pivot:
                a[i], a[j] = a[j], a[i]
                break

    for i in reversed(range(len(a))):
        for j in reversed(range(i)):
            if a[j] > pivot:
                a[i], a[j] = a[j], a[i]
                break

def dutch_flag_partition_2(a, pivot_index):
    pivot = a[pivot_index]

    # first pass: move smaller items to front
    smaller = 0 # pointer to location to put smaller item
    for i in range(len(a)):
        if a[i] < pivot:
            a[i], a[smaller] = a[smaller], a[i]
            smaller += 1

    # second pass: move larger items to front
    larger = len(a) - 1 # pointer to location to put larger item
    for i in reversed(range(len(a))):
        if a[i] > pivot:
            a[i], a[larger] = a[larger], a[i]
            larger -= 1

a  = [6, 7, 5, 4, 3, 2, 1, 8]
# dutch_flag_partition_1(a,3)
dutch_flag_partition_2(a,3)
print(a)
