# Input: array of digits encoding a nonnegative interger D, for example 129
# update the array to represent integer D+1, for example 130

def plus_one(a):
    a[-1] += 1
    for i in reversed(range(1,len(a))):
        if a[i] != 10:
            break
        a[i] = 0
        a[i-1] += 1

    # check the beginning
    if a[0] == 10: # happens when all the digits in a are 9s
        a[0] = 1
        a.append(0)

    return a

# a = [1, 2, 3, 9]
a = [9, 9, 9, 9]
print(plus_one(a))
