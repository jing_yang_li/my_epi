from scipy.optimize import linprog

# coefficients for objective z = x + 2y
obj = [-1, -2]

# inequality constraints
lhs_ineq = [[ 2,  1],  # Red constraint left side
            [-4,  5],  # Blue constraint left side
            [ 1, -2]]  # Yellow constraint left side

rhs_ineq = [20,  # Red constraint right side
            10,  # Blue constraint right side
            2]  # Yellow constraint right side

# equality constraints
lhs_eq = [[-1, 5]]  # Green constraint left side
rhs_eq = [15]       # Green constraint right side

# bounds for each variable
bnd = [(0, float("inf")),  # Bounds of x
       (0, float("inf"))]  # Bounds of y

# optimize and solve your problem
opt = linprog(c=obj, A_ub=lhs_ineq, b_ub=rhs_ineq,
              A_eq=lhs_eq, b_eq=rhs_eq, bounds=bnd,
              method="revised simplex")
print(opt)