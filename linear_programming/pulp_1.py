from pulp import LpMaximize, LpProblem, LpStatus, lpSum, LpVariable

# Create the model
model = LpProblem(name="small-problem", sense=LpMaximize)
# use the sense parameter to choose whether to perform minimization (LpMinimize or 1, which is the default)
# or maximization (LpMaximize or -1).

# define the decision variables as instances of the LpVariable class:
# Initialize the decision variables
x = LpVariable(name="x", lowBound=0)
# x = LpVariable(name="x", lowBound=0, cat="Integer")
y = LpVariable(name="y", lowBound=0)

# use the variables x and y to create other PuLP objects that represent linear expressions and constraints
expression = 2 * x + 4 * y
print(expression)
constraint = 2 * x + 4 * y >= 8
print(constraint)

# Add the constraints to the model
model += (2 * x + y <= 20, "red_constraint")
model += (4 * x - 5 * y >= -10, "blue_constraint")
model += (-x + 2 * y >= -2, "yellow_constraint")
model += (-x + 5 * y == 15, "green_constraint")
# In the above code, you define tuples that hold the constraints and their names. LpProblem allows you to add
# constraints to a model by specifying them as tuples. The first element is a LpConstraint instance. The second
# element is a human-readable name for that constraint.


# Add the objective function to the model
obj_func = x + 2 * y
model += obj_func
''' OR
model += x + 2 * y
'''

print("full definition of this model")
print(model)

# Solve the problem
status = model.solve()
print("results=================")
print(f"status: {model.status}, {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")
for var in model.variables():
    print(f"{var.name}: {var.value()}")
for name, constraint in model.constraints.items():
    print(f"{name}: {constraint.value()}")